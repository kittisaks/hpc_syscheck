
all:
	$(MAKE) -C matbin
	$(MAKE) -C nbbin
	$(MAKE) -C pthread
	$(MAKE) -C openmp
	$(MAKE) -C mpi

clean:
	$(MAKE) -C matbin clean
	$(MAKE) -C nbbin clean
	$(MAKE) -C pthread clean
	$(MAKE) -C openmp clean
	$(MAKE) -C mpi clean
