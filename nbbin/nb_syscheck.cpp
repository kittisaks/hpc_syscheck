#include <stdlib.h>

#include <iostream>

#include "nbbin.h"

using namespace std;

#define BODY_COUNT 25678
#define TIME_DELTA 0.125
#define MAX_NUMBER 9999.99

inline void checkError(int rc, const char * errMsg) {
    if (rc) {
        cerr << "[Error]: " << errMsg << endl;
        exit(-1);
    }
}

inline void checkNullError(void ** ptr, void * rc, const char * errMsg) {
    if (rc == NULL) {
        cerr << "[Error]: " << errMsg << endl;
        exit(-1);
    }

    *ptr = rc;
}

inline void initializeSnapshot(nbSnapshot_t * pss, uint32_t bodyCnt) {
    pss->posX = new dtype [bodyCnt];
    pss->posY = new dtype [bodyCnt];
    pss->posZ = new dtype [bodyCnt];
    pss->velX = new dtype [bodyCnt];
    pss->velY = new dtype [bodyCnt];
    pss->velZ = new dtype [bodyCnt];
    pss->accX = new dtype [bodyCnt];
    pss->accY = new dtype [bodyCnt];
    pss->accZ = new dtype [bodyCnt];
}

int main(int argc, char ** argv) {

    nbDataset_t ds;
    nbSnapshot_t rss;
    
    checkNullError((void **) &ds, (void *) nbLoadDatasetFromFile("refnb.bin"),
        "Cannot load reference dataset from file");

    initializeSnapshot(&rss, BODY_COUNT);

    dtype number = 0.0;
    dtype fraction = 0.0;
    for (uint32_t bIdx=0;bIdx<BODY_COUNT;bIdx++) {
        dtype element = number + fraction;
        number += 1.0;
        fraction += 0.1;
        if (number >= MAX_NUMBER)
            number = 0.0;
        if (fraction >= 1.0)
            fraction = 0.0;

        rss.posX[bIdx] = element;
        rss.posY[bIdx] = 2.0 * element;
        rss.posZ[bIdx] = 3.0 * element;
        rss.velX[bIdx] = 4.0 * element;
        rss.velY[bIdx] = 5.0 * element;
        rss.velZ[bIdx] = 6.0 * element;
        rss.accX[bIdx] = 7.0 * element;
        rss.accY[bIdx] = 8.0 * element;
        rss.accZ[bIdx] = 9.0 * element;
    }

    cout << "NBBIN Verification: ";

#define assertEqual(comp, ref, errMsg)                    \
    do {                                                  \
        if (comp != ref) {                                \
            cout << "FAILED (" << errMsg << ")." << endl; \
            exit(-1);                                     \
        }                                                 \
    } while(0)
    nbFileHeader_t fh = ds->fileHeader;
    assertEqual(fh.bodyCnt, BODY_COUNT, "body count");
    assertEqual(fh.timeDelta, TIME_DELTA, "time delta");
    assertEqual(fh.snapshotCnt, 1, "snapshot count");
    assertEqual(ds->snapshots->size(), 1, "snapshot-v count");
    assertEqual(ds->snapshotHeaders->size(), 1, "snapshot-h count");

    nbSnapshot_t * css = ds->snapshots->at(0);
    for (uint32_t bIdx=0;bIdx<BODY_COUNT;bIdx++) {
        assertEqual(css->posX[bIdx], rss.posX[bIdx], "X position");
        assertEqual(css->posY[bIdx], rss.posY[bIdx], "Y position");
        assertEqual(css->posZ[bIdx], rss.posZ[bIdx], "Z position");
        assertEqual(css->velX[bIdx], rss.velX[bIdx], "X velocity");
        assertEqual(css->velY[bIdx], rss.velY[bIdx], "Y velocity");
        assertEqual(css->velZ[bIdx], rss.velZ[bIdx], "Z velocity");
        assertEqual(css->accX[bIdx], rss.accX[bIdx], "X acceleration");
        assertEqual(css->accY[bIdx], rss.accY[bIdx], "Y acceleration");
        assertEqual(css->accZ[bIdx], rss.accZ[bIdx], "Z acceleration");
    }

#undef assertEqual

    cout << "PASSED" << endl;

    nbDestroyDataset(ds);

    return 0;
}


