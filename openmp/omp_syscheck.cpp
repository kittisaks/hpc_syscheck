#include <omp.h>
#include <iostream>

using namespace std;

#define NUM_THREADS 5

int main(int argc, char ** argv) {

    omp_set_num_threads(NUM_THREADS);

    #pragma omp parallel for
    for (int i=0;i<NUM_THREADS;i++) {
        cout << "[Thread-" << omp_get_thread_num() << "]: Hello!!!" << endl;
    }

    cout << endl << endl << "[Master]" << " ==================== Implcit barrier ====================" << endl << endl;

    #pragma omp parallel for
    for (int i=0;i<NUM_THREADS;i++) {
        cout << "[Thread-" << omp_get_thread_num() << "]: Message-1" << endl;
    }

    cout << endl << endl << "[Master]" << "==================== Implcit barrier ====================" << endl << endl;

    #pragma omp parallel for
    for (int i=0;i<NUM_THREADS;i++) {
        cout << "[Thread-" << omp_get_thread_num() << "]: Message-2" << endl;
    }

    cout << endl << endl << "[Master]" << "==================== Implcit barrier ====================" << endl << endl;

    #pragma omp parallel for
    for (int i=0;i<NUM_THREADS;i++) {
        cout << "[Thread-" << omp_get_thread_num() << "]: Message-3" << endl;
    }

    cout << "OpenMP Verification: PASSED" << endl;

    return 0;
}

