#include <stdlib.h>

#include <iostream>

#include "matbin.h"

using namespace std;

#define DIM_Y 500
#define DIM_X 1000
#define MAX_NUMBER 99999999.999

inline void checkError(int rc, const char * errMsg) {
    if (rc) {
        cerr << "[Error]: " << errMsg << endl;
        exit(-1);
    }
}

int main(int argc, char ** argv) {

    dtype ** compMat, ** refMat;
    unsigned int dimX, dimY;

    checkError(readMatBinaryFile("refmat.bin", &compMat, &dimY, &dimX),
        "Cannot load comparing matrix from file.");

    checkError(createMat(&refMat, DIM_Y, DIM_X),
        "Cannot create reference matrix.");

    dtype number = 0.0;
    dtype fraction = 0.0;
    for (unsigned int y=0;y<DIM_Y;y++) {
        for (unsigned int x=0;x<DIM_X;x++) {
            refMat[y][x] = number + fraction;
            number += 1.0;
            fraction += 0.1;
            if (number >= MAX_NUMBER)
                number = 0.0;
            if (fraction >= 1.0)
                fraction = 0.0;
        }
    }

    cout << "MatBin Verification: ";
    bool isPassed = true;

    if ((dimY != DIM_Y) || (dimX != DIM_X))
        isPassed = false;

    for (unsigned int y=0;y<DIM_Y;y++) {
        for (unsigned int x=0;x<DIM_X;x++) {
            if (compMat[y][x] != refMat[y][x]) {
                isPassed = false;
                break;
            }   
        }
        if (!isPassed)
            break;
    }
    if (isPassed)
        cout << "PASSED" << endl;
    else
        cout << "FAILED" << endl;

    checkError(destroyMat(compMat), "Cannot destroy comparing matrix.");
    checkError(destroyMat(refMat), "Cannot destroy reference matrix.");

    return 0;
}

