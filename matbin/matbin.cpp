#include <fstream>
#include "matbin.h"

using namespace std;

typedef unsigned int uint;

typedef struct {
    uint dimY;
    uint dimX;
} matHeader_t;

int createMat(dtype *** mat, uint dimY, uint dimX) {

    uint blockCnt = dimY * dimX;
    dtype * flat = new dtype [blockCnt];
    dtype ** tmat = new dtype * [dimY];

    uint blockIdx = 0;
    for (uint i=0;i<dimY;i++) {
        tmat[i] = flat + blockIdx;
        blockIdx += dimX;
    }

    *mat = tmat;

    return 0;
}

int destroyMat(dtype ** mat) {

    delete [] mat[0];
    delete [] mat;

    return 0;
}

int writeMatBinaryFile(const char * filename, dtype ** mat, uint dimY, uint dimX) {

    fstream file;
    matHeader_t header;

    header.dimY = dimY;
    header.dimX = dimX;

    file.open(filename, ios::out | ios::binary);
    if (!file.is_open())
        return -1;

    file.write(reinterpret_cast<char *>(&header), sizeof(matHeader_t));
    file.write(reinterpret_cast<char *>(mat[0]), dimY * dimX * sizeof(dtype));
    file.close();

    return 0;
}

int readMatBinaryFile(const char * filename, dtype *** mat, uint * dimY, uint * dimX) {

    fstream file;
    matHeader_t header;

    file.open(filename, ios::in | ios::binary);
    if (!file.is_open())
        return -1;

    file.read(reinterpret_cast<char *>(&header), sizeof(matHeader_t));

    //TODO: Handle negative dimension and other exceptions?

    dtype ** tmat;
    if (createMat(&tmat, header.dimY, header.dimX) != 0)
        return -1;
    file.read(reinterpret_cast<char *>(tmat[0]), header.dimY * header.dimX * sizeof(dtype));

    *mat = tmat;
    *dimY = header.dimY;
    *dimX = header.dimX;

    return 0;
}

