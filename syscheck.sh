#!/bin/bash
set -e
make all
openmp/omp_syscheck
pthread/pthread_syscheck
matbin/matbin_syscheck
nbbin/nb_syscheck
